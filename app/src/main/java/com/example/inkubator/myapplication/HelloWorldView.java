package com.example.inkubator.myapplication;

import com.arellomobile.mvp.MvpView;

/**
 * Created by inkubator on 06.07.17.
 */

public interface HelloWorldView extends MvpView {
    void showMessage(int message);
}
