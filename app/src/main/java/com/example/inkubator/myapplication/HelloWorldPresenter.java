package com.example.inkubator.myapplication;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

/**
 * Created by inkubator on 06.07.17.
 */

@InjectViewState
public class HelloWorldPresenter extends MvpPresenter<HelloWorldView> {
    public HelloWorldPresenter() {
        getViewState().showMessage(R.string.hello_world);
    }
}
